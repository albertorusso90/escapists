package com.polygonprojects.escapists;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.polygonprojects.escapists.di.activity.ActivityComponentBuilder;
import com.polygonprojects.escapists.di.activity.HasActivitySubcomponentBuilders;
import com.polygonprojects.escapists.di.app.AppComponent;
import com.polygonprojects.escapists.di.app.AppModule;
import com.polygonprojects.escapists.di.app.DaggerAppComponent;
import com.polygonprojects.escapists.di.fragments.FragmentComponentBuilder;
import com.polygonprojects.escapists.di.fragments.HasFragmentSubcomponentBuilders;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * Created by Alberto Russo on 31/05/17
 */

public class MyApplication extends Application implements HasActivitySubcomponentBuilders, HasFragmentSubcomponentBuilders {

    private AppComponent appComponent;

    @Inject
    Map<Class<? extends Activity>, Provider<ActivityComponentBuilder>> activityComponentBuilders;

    @Inject
    Map<Class<? extends Fragment>, Provider<FragmentComponentBuilder>> fragmentComponentBuilders;

    @Override
    public void onCreate() {
        super.onCreate();

        initializeDagger();
    }

    private void initializeDagger() {
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        appComponent.inject(this);
    }

    public static HasActivitySubcomponentBuilders getActivityBuilder(Context context) {
        return ((HasActivitySubcomponentBuilders) context.getApplicationContext());
    }

    public static HasFragmentSubcomponentBuilders getFragmentBuilder(Context context) {
        return ((HasFragmentSubcomponentBuilders) context.getApplicationContext());
    }

    @Override
    public ActivityComponentBuilder getActivityComponentBuilder(Class<? extends Activity> activityClass) {
        return activityComponentBuilders.get(activityClass).get();
    }

    @Override
    public FragmentComponentBuilder getFragmentComponentBuilder(Class<? extends Fragment> fragmentClass) {
        return fragmentComponentBuilders.get(fragmentClass).get();
    }
}

