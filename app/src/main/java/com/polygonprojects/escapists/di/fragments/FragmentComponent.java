package com.polygonprojects.escapists.di.fragments;

import android.support.v4.app.Fragment;

import dagger.MembersInjector;

/**
 * Created by Alberto Russo on 31/05/17.
 */

public interface FragmentComponent<F extends Fragment> extends MembersInjector<F> {
}
