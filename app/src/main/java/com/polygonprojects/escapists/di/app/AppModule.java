package com.polygonprojects.escapists.di.app;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.polygonprojects.escapists.BuildConfig;
import com.polygonprojects.escapists.networking.NetworkService;
import com.polygonprojects.escapists.networking.Service;
import com.polygonprojects.escapists.storage.UserSession;

import java.io.File;
import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by Alberto Russo on 31/05/17.
 */

@Module
public class AppModule {
    private UserSession mUserSession;
    private File mCacheFile;

    public AppModule(Application application) {
        Context context = application.getApplicationContext();

        mUserSession = new UserSession(context);
        mCacheFile = new File(application.getCacheDir(), "responses");
    }

    @Singleton
    @Provides
    UserSession provideUserSession() {
        return mUserSession;
    }

    @Singleton
    @Provides
    Retrofit provideCall() {
        Cache cache = null;

        try {
            cache = new Cache(mCacheFile, 10 * 1024 * 1024);
        } catch (Exception e) {
            e.printStackTrace();
        }

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        //Customize the request
                        Request.Builder builder = original.newBuilder();
                        builder.header("Content-Type", "application/json");
                        builder.removeHeader("Pragma");
                        builder.header("Cache-Control", String.format("max-age=%d", BuildConfig.CACHETIME));

                        String url = original.url().toString();
                        if(!url.contains("/login") && !url.contains("/register")){
                            builder.header("Authorization", mUserSession.getToken());
                        }

                        //Build the request
                        Request request = builder.build();

                        Response response = chain.proceed(request);
                        response.cacheResponse();
                        // Customize or return the response
                        return response;
                    }
                })
                .cache(cache)
                .build();

        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .setLenient()
                .create();

        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    @Singleton
    @Provides
    public NetworkService providesNetworkService(Retrofit retrofit) {
        return retrofit.create(NetworkService.class);
    }

    @Singleton
    @Provides
    public Service providesService(NetworkService networkService) {
        return new Service(networkService);
    }
}
