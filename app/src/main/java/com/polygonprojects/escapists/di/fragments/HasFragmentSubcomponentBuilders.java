package com.polygonprojects.escapists.di.fragments;

import android.support.v4.app.Fragment;

/**
 * Created by Alberto Russo on 31/05/17.
 */

public interface HasFragmentSubcomponentBuilders {
    FragmentComponentBuilder getFragmentComponentBuilder(Class<? extends Fragment> fragmentClass);
}
