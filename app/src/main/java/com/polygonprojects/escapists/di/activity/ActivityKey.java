package com.polygonprojects.escapists.di.activity;

import android.app.Activity;

import dagger.MapKey;

/**
 * Created by Alberto Russo on 31/05/17.
 */

@MapKey
public @interface ActivityKey {
    Class<? extends Activity> value();
}
