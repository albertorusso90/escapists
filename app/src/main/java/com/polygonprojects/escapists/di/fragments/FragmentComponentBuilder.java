package com.polygonprojects.escapists.di.fragments;

/**
 * Created by Alberto Russo on 31/05/17.
 */

public interface FragmentComponentBuilder<M extends FragmentModule, C extends FragmentComponent> {
    FragmentComponentBuilder<M, C> fragmentModule(M fragmentModule);
    C build();
}
