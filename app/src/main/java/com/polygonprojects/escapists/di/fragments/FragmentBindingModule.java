package com.polygonprojects.escapists.di.fragments;

import com.polygonprojects.escapists.di.components.ComponentProfile;
import com.polygonprojects.escapists.di.components.ComponentRooms;
import com.polygonprojects.escapists.presentation.ui.fragments.FragmentProfile;
import com.polygonprojects.escapists.presentation.ui.fragments.FragmentRooms;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created by Alberto Russo on 31/05/17.
 */

@Module(
        subcomponents = {
                ComponentProfile.class,
                ComponentRooms.class
        }
)
public abstract class FragmentBindingModule {

    @Binds
    @IntoMap
    @FragmentKey(FragmentProfile.class)
    public abstract FragmentComponentBuilder fragmentProfileComponentBuilder(ComponentProfile.Builder impl);

    @Binds
    @IntoMap
    @FragmentKey(FragmentRooms.class)
    public abstract FragmentComponentBuilder fragmentRoomsComponentBuilder(ComponentRooms.Builder impl);
}
