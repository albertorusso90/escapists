package com.polygonprojects.escapists.di.fragments;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Alberto Russo on 31/05/17.
 */

@Module
public abstract class FragmentModule<T> {
    protected final T fragment;

    public FragmentModule(T fragment) {
        this.fragment = fragment;
    }

    @Provides
    @FragmentScope
    public T provideFragment() {
        return fragment;
    }
}
