package com.polygonprojects.escapists.di.activity;

import android.app.Activity;

import dagger.MembersInjector;

/**
 * Created by Alberto Russo on 31/05/17.
 */

public interface ActivityComponent<A extends Activity> extends MembersInjector<A> {
}
