package com.polygonprojects.escapists.di.app;

import com.polygonprojects.escapists.MyApplication;
import com.polygonprojects.escapists.di.activity.ActivityBindingModule;
import com.polygonprojects.escapists.di.fragments.FragmentBindingModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Alberto Russo on 31/05/17.
 */

@Singleton
@Component(
        modules = {
                AppModule.class,
                ActivityBindingModule.class,
                FragmentBindingModule.class
        }
)
public interface AppComponent {
    MyApplication inject(MyApplication application);
}
