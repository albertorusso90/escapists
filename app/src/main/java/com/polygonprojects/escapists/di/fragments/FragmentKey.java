package com.polygonprojects.escapists.di.fragments;

import android.support.v4.app.Fragment;

import dagger.MapKey;

/**
 * Created by Alberto Russo on 31/05/17.
 */

@MapKey
public @interface FragmentKey {
    Class<? extends Fragment> value();
}
