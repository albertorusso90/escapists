package com.polygonprojects.escapists.di.components;

import com.polygonprojects.escapists.di.activity.ActivityComponent;
import com.polygonprojects.escapists.di.activity.ActivityComponentBuilder;
import com.polygonprojects.escapists.di.activity.ActivityModule;
import com.polygonprojects.escapists.di.activity.ActivityScope;
import com.polygonprojects.escapists.presentation.ui.activities.ActivityMain;

import dagger.Module;
import dagger.Subcomponent;

/**
 * Created by Alberto Russo on 31/05/17.
 */

@ActivityScope
@Subcomponent(modules = ComponentMain.ModuleMain.class)
public interface ComponentMain extends ActivityComponent<ActivityMain> {

    @Subcomponent.Builder
    interface Builder extends ActivityComponentBuilder<ModuleMain, ComponentMain> {}

    @Module
    class ModuleMain extends ActivityModule<ActivityMain> {
        public ModuleMain(ActivityMain activity) {
            super(activity);
        }
    }

}
