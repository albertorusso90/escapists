package com.polygonprojects.escapists.di.components;

import com.polygonprojects.escapists.di.fragments.FragmentComponent;
import com.polygonprojects.escapists.di.fragments.FragmentComponentBuilder;
import com.polygonprojects.escapists.di.fragments.FragmentModule;
import com.polygonprojects.escapists.di.fragments.FragmentScope;
import com.polygonprojects.escapists.presentation.ui.fragments.FragmentProfile;

import dagger.Module;
import dagger.Subcomponent;

/**
 * Created by Alberto Russo on 31/05/17.
 */

@FragmentScope
@Subcomponent(modules = ComponentProfile.ModuleProfile.class)
public interface ComponentProfile extends FragmentComponent<FragmentProfile> {

    @Subcomponent.Builder
    interface Builder extends FragmentComponentBuilder<ModuleProfile, ComponentProfile> {}

    @Module
    class ModuleProfile extends FragmentModule<FragmentProfile> {
        public ModuleProfile(FragmentProfile fragment) {
            super(fragment);
        }
    }
}
