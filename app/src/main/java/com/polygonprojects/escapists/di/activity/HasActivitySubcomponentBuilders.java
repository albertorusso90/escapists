package com.polygonprojects.escapists.di.activity;

import android.app.Activity;

/**
 * Created by Alberto Russo on 31/05/17.
 */

public interface HasActivitySubcomponentBuilders {
    ActivityComponentBuilder getActivityComponentBuilder(Class<? extends Activity> activityClass);
}
