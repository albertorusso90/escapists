package com.polygonprojects.escapists.di.activity;


import com.polygonprojects.escapists.di.components.ComponentMain;
import com.polygonprojects.escapists.di.components.ComponentSign;
import com.polygonprojects.escapists.di.components.ComponentSplash;
import com.polygonprojects.escapists.presentation.ui.activities.ActivityMain;
import com.polygonprojects.escapists.presentation.ui.activities.ActivitySign;
import com.polygonprojects.escapists.presentation.ui.activities.ActivitySplash;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created by Alberto Russo on 31/05/17.
 */

@Module(
        subcomponents = {
                ComponentSplash.class,
                ComponentSign.class,
                ComponentMain.class
        }
)
public abstract class ActivityBindingModule {

    @Binds
    @IntoMap
    @ActivityKey(ActivitySplash.class)
    public abstract ActivityComponentBuilder activitySplashComponentBuilder(ComponentSplash.Builder impl);

    @Binds
    @IntoMap
    @ActivityKey(ActivitySign.class)
    public abstract ActivityComponentBuilder activitySignComponentBuilder(ComponentSign.Builder impl);

    @Binds
    @IntoMap
    @ActivityKey(ActivityMain.class)
    public abstract ActivityComponentBuilder activityMainComponentBuilder(ComponentMain.Builder impl);
}
