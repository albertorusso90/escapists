package com.polygonprojects.escapists.di.activity;

/**
 * Created by Alberto Russo on 31/05/17.
 */

public interface ActivityComponentBuilder<M extends ActivityModule, C extends ActivityComponent> {
    ActivityComponentBuilder<M, C> activityModule(M activityModule);
    C build();
}