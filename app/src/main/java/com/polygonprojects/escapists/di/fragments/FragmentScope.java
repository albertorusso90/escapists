package com.polygonprojects.escapists.di.fragments;

import javax.inject.Scope;

/**
 * Created by Alberto Russo on 31/05/17.
 */

@Scope
public @interface FragmentScope {
}
