package com.polygonprojects.escapists.di.components;

import com.polygonprojects.escapists.di.activity.ActivityComponent;
import com.polygonprojects.escapists.di.activity.ActivityComponentBuilder;
import com.polygonprojects.escapists.di.activity.ActivityModule;
import com.polygonprojects.escapists.di.activity.ActivityScope;
import com.polygonprojects.escapists.presentation.ui.activities.ActivitySplash;

import dagger.Module;
import dagger.Subcomponent;

/**
 * Created by Alberto Russo on 31/05/17.
 */

@ActivityScope
@Subcomponent(modules = ComponentSplash.ModuleSplash.class)
public interface ComponentSplash extends ActivityComponent<ActivitySplash> {

    @Subcomponent.Builder
    interface Builder extends ActivityComponentBuilder<ModuleSplash, ComponentSplash> {}

    @Module
    class ModuleSplash extends ActivityModule<ActivitySplash> {
        public ModuleSplash(ActivitySplash activity) {
            super(activity);
        }
    }
}
