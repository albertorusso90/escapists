package com.polygonprojects.escapists.di.components;

import com.polygonprojects.escapists.di.activity.ActivityComponent;
import com.polygonprojects.escapists.di.activity.ActivityComponentBuilder;
import com.polygonprojects.escapists.di.activity.ActivityModule;
import com.polygonprojects.escapists.di.activity.ActivityScope;
import com.polygonprojects.escapists.presentation.ui.activities.ActivitySign;

import dagger.Module;
import dagger.Subcomponent;

/**
 * Created by Alberto Russo on 02/06/17
 */

@ActivityScope
@Subcomponent(modules = ComponentSign.ModuleSign.class)
public interface ComponentSign extends ActivityComponent<ActivitySign> {

    @Subcomponent.Builder
    interface Builder extends ActivityComponentBuilder<ModuleSign, ComponentSign> {}

    @Module
    class ModuleSign extends ActivityModule<ActivitySign> {
        public ModuleSign(ActivitySign activitySign) {
            super(activitySign);
        }
    }
}
