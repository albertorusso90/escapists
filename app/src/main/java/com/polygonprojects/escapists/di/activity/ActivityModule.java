package com.polygonprojects.escapists.di.activity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Alberto Russo on 31/05/17.
 */

@Module
public abstract class ActivityModule<T> {
    protected final T activity;

    public ActivityModule(T activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    public T provideActivity() {
        return activity;
    }
}
