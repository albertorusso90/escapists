package com.polygonprojects.escapists.di.components;

import com.polygonprojects.escapists.di.fragments.FragmentComponent;
import com.polygonprojects.escapists.di.fragments.FragmentComponentBuilder;
import com.polygonprojects.escapists.di.fragments.FragmentModule;
import com.polygonprojects.escapists.di.fragments.FragmentScope;
import com.polygonprojects.escapists.presentation.ui.fragments.FragmentRooms;

import dagger.Module;
import dagger.Subcomponent;

/**
 * Created by Alberto Russo on 02/06/17
 */

@FragmentScope
@Subcomponent(modules = ComponentRooms.ModuleRooms.class)
public interface ComponentRooms extends FragmentComponent<FragmentRooms> {

    @Subcomponent.Builder
    interface Builder extends FragmentComponentBuilder<ModuleRooms, ComponentRooms> {}

    @Module
    class ModuleRooms extends FragmentModule<FragmentRooms> {
        public ModuleRooms(FragmentRooms fragment) {
            super(fragment);
        }
    }
}