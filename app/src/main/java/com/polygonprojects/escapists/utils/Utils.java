package com.polygonprojects.escapists.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.polygonprojects.escapists.R;

/**
 * Created by Alberto Russo on 01/06/17
 */

public class Utils {

    public static void toast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public static void openFragment(FragmentManager fragmentManager, Fragment fragment) {
        openFragment(fragmentManager, fragment, false);
    }

    public static void openFragment(FragmentManager fragmentManager, Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.replace(R.id.fragment_container, fragment, fragment.getClass().getSimpleName());

        if(addToBackStack) {
            transaction.addToBackStack(fragment.getTag());
        }

        transaction.commitAllowingStateLoss();
    }

    public static void openActivity(Activity activity, Class c) {
        openActivity(activity, c, false);
    }

    public static void openActivity(Activity activity, Intent intent) {
        openActivity(activity, intent, false);
    }

    public static void openActivity(Activity activity, Class c, boolean terminate) {
        openActivity(activity, new Intent(activity, c), terminate);
    }

    public static void openActivity(Activity activity, Intent intent, boolean terminate) {
        activity.startActivity(intent);
        activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        if(terminate) {
            activity.finish();
        }
    }

    public static void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager)view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
