package com.polygonprojects.escapists.utils;

/**
 * Created by Alberto Russo on 21/03/17.
 */

public class Constants {

    public static final String KEY_EMAIL = "emailAddress";
    public static final String KEY_FIRST_NAME = "firstName";
    public static final String KEY_LAST_NAME = "lastName";
    public static final String KEY_PASSWORD = "password";

    public static final String KEY_NAME = "name";

    public static final String ARG_ROOM = "arg_room";
}
