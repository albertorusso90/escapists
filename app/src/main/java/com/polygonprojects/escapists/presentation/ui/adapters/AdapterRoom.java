package com.polygonprojects.escapists.presentation.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.polygonprojects.escapists.R;
import com.polygonprojects.escapists.models.Room;
import com.polygonprojects.escapists.presentation.ui.fragments.FragmentRooms;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Alberto Russo on 02/06/17
 */

public class AdapterRoom extends RecyclerView.Adapter<AdapterRoom.ViewHolder> {

    private List<Room> mItems = new ArrayList<>();
    private FragmentRooms.Callback mCallback;

    public AdapterRoom(FragmentRooms.Callback callback) {
        mCallback = callback;
    }

    public void addItems(List<Room> rooms) {
        mItems.clear();
        mItems.addAll(rooms);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_room, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.init(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private Room mRoom;

        @InjectView(R.id.textTitle) TextView mTextTitle;
        @InjectView(R.id.textDescription) TextView mTextDescription;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }

        void init(Room room) {
            mTextTitle.setText(room.getName());
            mTextDescription.setText(room.getDescription());

            mRoom = room;
        }

        @OnClick(R.id.buttonOpen) public void onOpenButtonClicked() {
            mCallback.onRoomSelected(mRoom);
        }
    }
}
