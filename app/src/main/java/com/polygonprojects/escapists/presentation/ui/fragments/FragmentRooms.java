package com.polygonprojects.escapists.presentation.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.polygonprojects.escapists.R;
import com.polygonprojects.escapists.di.components.ComponentRooms;
import com.polygonprojects.escapists.di.fragments.HasFragmentSubcomponentBuilders;
import com.polygonprojects.escapists.models.Room;
import com.polygonprojects.escapists.presentation.presenters.PresenterRooms;
import com.polygonprojects.escapists.presentation.ui.base.BaseFragment;
import com.polygonprojects.escapists.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Alberto Russo on 03/06/17
 */

public class FragmentRooms extends BaseFragment implements PresenterRooms.Callback.View {

    private static final String TAG = FragmentRooms.class.getSimpleName();

    private List<Room> mRooms = new ArrayList<>();

    @Inject PresenterRooms mPresenter;

    @InjectView(R.id.progressBar) ProgressBar mProgressBar;
    @InjectView(R.id.tabLayout) TabLayout mTabLayout;
    @InjectView(R.id.viewPager) ViewPager mViewPager;

    public interface Callback {
        void onRoomSelected(Room room);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rooms, container, false);
        ButterKnife.inject(this, view);

        hideProgress();

        mViewPager.setAdapter(new MyPagerAdapter(getFragmentManager()));
        mTabLayout.setupWithViewPager(mViewPager);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        getActivity().setTitle(R.string.ROOMS_TITLE);

        mPresenter.getRooms();
    }

    @Override
    protected void injectMembers(HasFragmentSubcomponentBuilders hasFragmentSubcomponentBuilders) {
        ((ComponentRooms.Builder) hasFragmentSubcomponentBuilders.getFragmentComponentBuilder(FragmentRooms.class))
                .fragmentModule(new ComponentRooms.ModuleRooms(this))
                .build()
                .injectMembers(this);
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        Utils.toast(getContext(), message);
    }

    @Override
    public void onRooms(List<Room> rooms) {
        mRooms = rooms;

        MyPagerAdapter adapter = (MyPagerAdapter)mViewPager.getAdapter();
        if(adapter != null) {
            Fragment fragment = adapter.getItem(mViewPager.getCurrentItem());
            if(fragment instanceof FragmentRoomList) {
                ((FragmentRoomList) fragment).refreshItems(mRooms);
            } else if (fragment instanceof FragmentRoomMap) {

            }
        }
    }

    public List<Room> getRooms() {
        return mRooms;
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        private List<Item> mItems = new ArrayList<>();

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);

            mItems.add(new Item(R.string.ROOM_LIST_TITLE, new FragmentRoomList()));
            mItems.add(new Item(R.string.ROOM_MAP_TITLE, new FragmentRoomMap()));
        }

        @Override
        public Fragment getItem(int position) {
            return mItems.get(position).fragment;
        }

        @Override
        public int getCount() {
            return mItems.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getString(mItems.get(position).titleId);
        }

        private class Item {
            private int titleId;
            private Fragment fragment;

            public Item(int titleId, Fragment fragment) {
                this.titleId = titleId;
                this.fragment = fragment;
            }
        }
    }
}
