package com.polygonprojects.escapists.presentation.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import com.polygonprojects.escapists.R;
import com.polygonprojects.escapists.utils.Constants;
import com.polygonprojects.escapists.utils.Utils;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnTextChanged;

/**
 * Created by Alberto Russo on 02/06/17
 */

public class FragmentSignIn extends Fragment {

    private static final String TAG = FragmentSignIn.class.getSimpleName();

    private Callback mCallback;

    @InjectView(R.id.textInputLayoutEmail) TextInputLayout mTextInputEmail;
    @InjectView(R.id.editEmail) EditText mEditEmail;
    @InjectView(R.id.textInputLayoutPassword) TextInputLayout mTextInputPassword;
    @InjectView(R.id.editPassword) EditText mEditPassword;
    @InjectView(R.id.buttonSignIn) Button mButtonSignIn;

    public interface Callback {
        void onSignIn(Map<String, String> fields);
        void onSignUpSelected();
        void onGuestSelected();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        ButterKnife.inject(this, view);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (Callback)context;
    }

    public void toggleButton(boolean enable) {
        mButtonSignIn.setEnabled(enable);
    }

    @OnEditorAction(R.id.editPassword) public boolean onEditorAction(int actionId) {
        if(actionId == EditorInfo.IME_ACTION_DONE) {
            onSignInButtonClicked();
            return true;
        } else {
            return false;
        }
    }

    @OnClick(R.id.buttonSignIn) public void onSignInButtonClicked() {
        Utils.hideKeyboard(mEditPassword);

        if(!validate()) {
            return;
        }

        String email = mEditEmail.getText().toString();
        String password = mEditPassword.getText().toString();

        Map<String, String> fields = new HashMap<>();
        fields.put(Constants.KEY_EMAIL, email);
        fields.put(Constants.KEY_PASSWORD, password);

        mCallback.onSignIn(fields);
    }

    @OnClick(R.id.buttonSignUp) public void onSignUpButtonClicked() {
        mCallback.onSignUpSelected();
    }

    @OnClick(R.id.buttonGuest) public void onGuestButtonClicked() {
        mCallback.onGuestSelected();
    }

    @OnTextChanged({R.id.editEmail, R.id.editPassword}) public void onTextChanged() {
        if(mTextInputEmail.getError() != null || mTextInputPassword.getError() != null) {
            mTextInputEmail.setError(null);
            mTextInputPassword.setError(null);
        }
    }

    private boolean validate() {
        boolean valid = true;

        String email = mEditEmail.getText().toString();
        String password = mEditPassword.getText().toString();

        if(email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mTextInputEmail.setError(getString(R.string.SIGN_IN_ERROR_EMAIL_INVALID));
            valid = false;
        } else {
            mTextInputEmail.setError(null);
        }

        if(password.isEmpty() || password.length() < 4 || password.length() > 12) {
            mTextInputPassword.setError(getString(R.string.SIGN_IN_ERROR_PASSWORD_INVALID));
            valid = false;
        } else {
            mTextInputPassword.setError(null);
        }

        return valid;
    }
}
