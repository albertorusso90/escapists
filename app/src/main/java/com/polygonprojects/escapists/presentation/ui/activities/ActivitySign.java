package com.polygonprojects.escapists.presentation.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ProgressBar;

import com.polygonprojects.escapists.R;
import com.polygonprojects.escapists.di.activity.HasActivitySubcomponentBuilders;
import com.polygonprojects.escapists.di.components.ComponentSign;
import com.polygonprojects.escapists.presentation.presenters.PresenterSign;
import com.polygonprojects.escapists.presentation.ui.base.BaseActivity;
import com.polygonprojects.escapists.presentation.ui.fragments.FragmentSignIn;
import com.polygonprojects.escapists.presentation.ui.fragments.FragmentSignUp;
import com.polygonprojects.escapists.utils.Utils;

import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Alberto Russo on 01/06/17
 */

public class ActivitySign extends BaseActivity implements FragmentSignIn.Callback, FragmentSignUp.Callback, PresenterSign.Callback.View{

    private static final String TAG = ActivitySign.class.getSimpleName();

    @Inject PresenterSign mPresenter;

    @InjectView(R.id.progressBar) ProgressBar mProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
        ButterKnife.inject(this);

        if(savedInstanceState == null) {
            hideProgress();

            Utils.openFragment(getSupportFragmentManager(), new FragmentSignIn());
        }
    }

    @Override
    protected void injectMembers(HasActivitySubcomponentBuilders hasActivitySubcomponentBuilders) {
        ((ComponentSign.Builder) hasActivitySubcomponentBuilders.getActivityComponentBuilder(ActivitySign.class))
                .activityModule(new ComponentSign.ModuleSign(this))
                .build()
                .injectMembers(this);
    }

    @Override
    public void onSignIn(Map<String, String> fields) {
        mPresenter.login(fields);
    }


    @Override
    public void onSignUp(Map<String, String> fields) {
        mPresenter.register(fields);
    }

    @Override
    public void onSignUpSelected() {
        Utils.openFragment(getSupportFragmentManager(), new FragmentSignUp(), true);
    }

    @Override
    public void onGuestSelected() {
        onSuccessSign();
    }

    @Override
    public void showProgress() {
        toggleButtons(true);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        toggleButtons(false);
        mProgressBar.setVisibility(View.GONE);
    }

    private void toggleButtons(boolean enable) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if(fragment instanceof FragmentSignIn) {
            ((FragmentSignIn) fragment).toggleButton(!enable);
        } else if(fragment instanceof FragmentSignUp) {
            ((FragmentSignUp) fragment).toggleButton(!enable);
        }
    }

    @Override
    public void showError(String message) {
        Utils.toast(this, message);
    }

    @Override
    public void onSuccessSign() {
        Utils.openActivity(this, ActivityMain.class, true);
    }
}
