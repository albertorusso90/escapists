package com.polygonprojects.escapists.presentation.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.polygonprojects.escapists.R;
import com.polygonprojects.escapists.models.Room;
import com.polygonprojects.escapists.presentation.ui.fragments.FragmentAbout;
import com.polygonprojects.escapists.presentation.ui.fragments.FragmentRooms;
import com.polygonprojects.escapists.utils.Constants;
import com.polygonprojects.escapists.utils.Utils;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ActivityMain extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, FragmentRooms.Callback {

    @InjectView(R.id.toolbar) Toolbar mToolbar;
    @InjectView(R.id.drawer_layout) DrawerLayout mDrawerLayout;
    @InjectView(R.id.nav_view) NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        init();

        if(savedInstanceState == null) {
            openRooms();
        }
    }

    private void init() {
        setSupportActionBar(mToolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        mNavigationView.setNavigationItemSelectedListener(this);

        initHeaderDrawer();
    }

    private void initHeaderDrawer() {
        View headerView = mNavigationView.getHeaderView(0);
    }

    @Override
    public void onRoomSelected(Room room) {
        Intent intent = new Intent(this, ActivityRoom.class);
        intent.putExtra(Constants.ARG_ROOM, room);

        Utils.openActivity(this, intent);
    }

    private void openRooms() {
        mNavigationView.setCheckedItem(R.id.nav_room);
        onNavigationItemSelected(mNavigationView.getMenu().findItem(R.id.nav_room));
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if(fragment instanceof FragmentRooms) {
                super.onBackPressed();
            } else {
                openRooms();
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        switch (item.getItemId()) {
            case R.id.nav_room:
                Utils.openFragment(getSupportFragmentManager(), new FragmentRooms());
                break;
            case R.id.nav_about:
                Utils.openFragment(getSupportFragmentManager(), new FragmentAbout());
                break;
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}
