package com.polygonprojects.escapists.presentation.presenters;

import android.os.Handler;

import com.polygonprojects.escapists.di.activity.ActivityScope;
import com.polygonprojects.escapists.models.User;
import com.polygonprojects.escapists.networking.Service;
import com.polygonprojects.escapists.presentation.ui.activities.ActivitySplash;
import com.polygonprojects.escapists.presentation.ui.base.BaseView;
import com.polygonprojects.escapists.storage.UserSession;

import javax.inject.Inject;

/**
 * Created by Alberto Russo on 01/06/17
 */

@ActivityScope
public class PresenterSplash {

    private static final long DEFAULT_DELAY = 3000;

    private final Callback mCallback;
    private Handler mHandler;
    private MyRunnable mRunnable;
    private long mTime;

    @Inject Service mService;
    @Inject UserSession mUserSession;

    public interface Callback extends BaseView {
        void onFinished(User user);
    }

    @Inject
    public PresenterSplash(ActivitySplash activitySplash) {
        this.mCallback = activitySplash;
        this.mHandler = new Handler();
    }

    public void onResume() {
        mCallback.showProgress();

        mTime = System.currentTimeMillis();

        if(mUserSession.hasUser()) {
            //TODO: SIGN IN USER
        } else {
            //we make the splash stay for at least 2 seconds to make the user think we doing something...
            startRunnable(null);
        }
    }

    public void onPause() {
        mCallback.hideProgress();

        if(mRunnable != null ) {
            mHandler.removeCallbacks(mRunnable);
        }
    }

    /**
     * This method will first calculate the amount of time we should display the splash screen for
     * depending on the amount of time it took to sign in the user or if it didnt happend.
     * The splash screen should be shown at least for 3 seconds.
     * @param user : User object, if null then user wasnt authenticated.
     */
    private void startRunnable(User user) {
        mTime = System.currentTimeMillis() - mTime;

        long delay = mTime < DEFAULT_DELAY ? (DEFAULT_DELAY - mTime) : 0;

        mRunnable = new MyRunnable(user);
        mHandler.postDelayed(mRunnable, delay);
    }

    private class MyRunnable implements Runnable {

        private User user;

        MyRunnable(User user) {
            this.user = user;
        }

        @Override
        public void run() {
            mCallback.hideProgress();
            mCallback.onFinished(user);
        }
    }
}
