package com.polygonprojects.escapists.presentation.ui.activities;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.polygonprojects.escapists.R;
import com.polygonprojects.escapists.di.activity.HasActivitySubcomponentBuilders;
import com.polygonprojects.escapists.di.components.ComponentSplash;
import com.polygonprojects.escapists.models.User;
import com.polygonprojects.escapists.networking.Service;
import com.polygonprojects.escapists.presentation.presenters.PresenterSplash;
import com.polygonprojects.escapists.presentation.ui.base.BaseActivity;
import com.polygonprojects.escapists.utils.Utils;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Alberto Russo on 01/06/17
 */

public class ActivitySplash extends BaseActivity implements PresenterSplash.Callback{

    private static final String TAG = ActivitySplash.class.getSimpleName();

    @Inject Service service;
    @Inject PresenterSplash mPresenter;

    @InjectView(R.id.relativeLayout) RelativeLayout mRelativeLayout;
    @InjectView(R.id.textView) TextView mTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Make screen full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);
        ButterKnife.inject(this);

        init();
    }

    private void init() {
        //Start animation process
        AnimationDrawable animationDrawable = (AnimationDrawable)mRelativeLayout.getBackground();
        animationDrawable.setEnterFadeDuration(0);
        animationDrawable.setExitFadeDuration(750);

        animationDrawable.start();
    }

    @Override
    protected void injectMembers(HasActivitySubcomponentBuilders hasActivitySubcomponentBuilders) {
        ((ComponentSplash.Builder) hasActivitySubcomponentBuilders.getActivityComponentBuilder(ActivitySplash.class))
                .activityModule(new ComponentSplash.ModuleSplash(this))
                .build()
                .injectMembers(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.onPause();
    }

    @Override
    public void showProgress() {
        Log.i(TAG, "show progress");
    }

    @Override
    public void hideProgress() {
        Log.i(TAG, "hide progress");
    }

    @Override
    public void showError(String message) {
        Utils.toast(getApplicationContext(), message);
    }

    @Override
    public void onFinished(User user) {
        Utils.openActivity(this, user != null ? ActivityMain.class : ActivitySign.class, true);
    }
}
