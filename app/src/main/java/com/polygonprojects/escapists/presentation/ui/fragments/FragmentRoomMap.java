package com.polygonprojects.escapists.presentation.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.polygonprojects.escapists.R;

import butterknife.ButterKnife;

/**
 * Created by Alberto Russo on 03/06/17
 */

public class FragmentRoomMap extends Fragment {

    private static final String TAG = FragmentRoomMap.class.getCanonicalName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_room_map, container, false);
        ButterKnife.inject(this, view);

        return view;
    }
}
