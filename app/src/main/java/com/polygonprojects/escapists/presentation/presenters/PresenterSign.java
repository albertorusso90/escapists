package com.polygonprojects.escapists.presentation.presenters;

import android.content.Context;

import com.polygonprojects.escapists.R;
import com.polygonprojects.escapists.models.User;
import com.polygonprojects.escapists.networking.NetworkError;
import com.polygonprojects.escapists.networking.Service;
import com.polygonprojects.escapists.presentation.ui.activities.ActivitySign;
import com.polygonprojects.escapists.presentation.ui.base.BaseView;
import com.polygonprojects.escapists.storage.UserSession;
import com.polygonprojects.escapists.utils.Constants;

import java.util.Map;

import javax.inject.Inject;

import retrofit2.Response;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Alberto Russo on 02/06/17
 */

public class PresenterSign {

    private final Callback.View mView;
    private CompositeSubscription mSubscriptions;
    private Context mContext;

    @Inject Service mService;
    @Inject UserSession mUserSession;

    public interface Callback {
        void onSuccess(Response<User> userResponse);
        void onError(NetworkError networkError);

        interface View extends BaseView {
            void onSuccessSign();
        }
    }

    @Inject
    public PresenterSign(ActivitySign activitySign) {
        this.mView = activitySign;
        this.mSubscriptions = new CompositeSubscription();
        this.mContext = activitySign;
    }

    public void login(final Map<String, String> fields) {
        mView.showProgress();

        Subscription subscription = mService.login(fields, new MyCallback(fields));

        mSubscriptions.add(subscription);
    }

    public void register(final Map<String, String> fields) {
        mView.showProgress();

        Subscription subscription = mService.register(fields, new MyCallback(fields));

        mSubscriptions.add(subscription);
    }

    public void onStop() {
        mSubscriptions.unsubscribe();
    }

    private class MyCallback implements Callback {

        private Map<String, String> mFields;

        private MyCallback(Map<String, String> fields) {
            mFields = fields;
        }

        @Override
        public void onSuccess(Response<User> userResponse) {
            String password = mFields.get(Constants.KEY_PASSWORD);
            String token = userResponse.headers().get("Authorization");

            User user = userResponse.body();

            //TODO: CHECK LOGIC IN HERE FOR THE FUTURE WITH REGARDS TO THE TOKEN...
            if(token == null || token.isEmpty()) {
                mView.hideProgress();
                //TODO: DISPLAY A BETTER ERROR MESSAGE HERE
                mView.showError("Token was invalid...");
            } else {
                mUserSession.saveUser(user, password, token);

                mView.hideProgress();
                mView.onSuccessSign();
            }
        }

        @Override
        public void onError(NetworkError networkError) {
            mView.hideProgress();

            //TODO: CHECK WHAT TYPE OF NETWORK ERROR IT IS. E.G. USER ALREADY EXISTS
            String message = networkError.getMessage();

            //TODO REMOVE THIS - Currently i dont have the API fully defined so i will show a random message to the user.
            message = mContext.getString(R.string.SIGN_IN_ERROR_API);

            mView.showError(message);
        }
    }
}
