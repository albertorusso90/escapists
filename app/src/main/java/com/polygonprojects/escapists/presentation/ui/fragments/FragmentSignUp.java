package com.polygonprojects.escapists.presentation.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import com.polygonprojects.escapists.R;
import com.polygonprojects.escapists.utils.Constants;
import com.polygonprojects.escapists.utils.Utils;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnTextChanged;

/**
 * Created by Alberto Russo on 02/06/17
 */

public class FragmentSignUp extends Fragment {

    private static final String TAG = FragmentSignUp.class.getSimpleName();

    private Callback mCallback;

    @InjectView(R.id.textInputEmail) TextInputLayout mTextInputEmail;
    @InjectView(R.id.editEmail) EditText mEditEmail;
    @InjectView(R.id.textInputFirstName) TextInputLayout mTextInputFirstName;
    @InjectView(R.id.editFirstName) EditText mEditFirstName;
    @InjectView(R.id.textInputLastName) TextInputLayout mTextInputLastName;
    @InjectView(R.id.editLastName) EditText mEditLastName;
    @InjectView(R.id.textInputPassword) TextInputLayout mTextInputPassword;
    @InjectView(R.id.editPassword) EditText mEditPassword;
    @InjectView(R.id.textInputConfirm) TextInputLayout mTextInputConfirm;
    @InjectView(R.id.editConfirm) EditText mEditConfirm;
    @InjectView(R.id.buttonSignUp) Button mButtonSignUp;

    public interface Callback {
        void onSignUp(Map<String, String> fields);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        ButterKnife.inject(this, view);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (Callback)context;
    }

    public void toggleButton(boolean enable) {
        mButtonSignUp.setEnabled(enable);
    }

    @OnEditorAction(R.id.editConfirm) public boolean onEditorAction(int actionId) {
        if(actionId == EditorInfo.IME_ACTION_DONE) {
            onSignUpButtonClicked();
            return true;
        } else {
            return false;
        }
    }

    @OnClick(R.id.buttonSignUp) public void onSignUpButtonClicked() {
        Utils.hideKeyboard(mEditConfirm);

        if(!validate()) {
            return;
        }

        String email = mEditEmail.getText().toString();
        String firstname = mEditFirstName.getText().toString();
        String lastname = mEditLastName.getText().toString();
        String password = mEditPassword.getText().toString();

        Map<String, String> fields = new HashMap<>();
        fields.put(Constants.KEY_EMAIL, email);
        fields.put(Constants.KEY_FIRST_NAME, firstname);
        fields.put(Constants.KEY_LAST_NAME, lastname);
        fields.put(Constants.KEY_PASSWORD, password);

        mCallback.onSignUp(fields);
    }

    @OnTextChanged({R.id.editEmail, R.id.editFirstName, R.id.editLastName, R.id.editPassword, R.id.editConfirm}) public void onTextChanged() {
        if(mTextInputEmail.getError() != null
                || mTextInputFirstName.getError() != null
                || mTextInputLastName.getError() != null
                || mTextInputPassword.getError() != null
                || mTextInputConfirm.getError() != null) {

            mTextInputEmail.setError(null);
            mTextInputFirstName.setError(null);
            mTextInputLastName.setError(null);
            mTextInputPassword.setError(null);
            mTextInputConfirm.setError(null);
        }
    }

    private boolean validate() {
        boolean valid = true;

        String email = mEditEmail.getText().toString();
        String firstname = mEditFirstName.getText().toString();
        String lastname = mEditLastName.getText().toString();
        String password = mEditPassword.getText().toString();
        String confirm = mEditConfirm.getText().toString();

        if(email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mTextInputEmail.setError(getString(R.string.SIGN_UP_ERROR_EMAIL_INVALID));
            valid = false;
        } else {
            mTextInputEmail.setError(null);
        }

        if(firstname.isEmpty() || firstname.length() < 4) {
            mTextInputFirstName.setError(getString(R.string.SIGN_UP_ERROR_FIRST_NAME_SHORT));
            valid = false;
        } else if(firstname.length() > 12) {
            mTextInputFirstName.setError(getString(R.string.SIGN_UP_ERROR_FIRST_NAME_LONG));
            valid = false;
        } else {
            mTextInputFirstName.setError(null);
        }

        if(lastname.isEmpty() || lastname.length() < 4) {
            mTextInputLastName.setError(getString(R.string.SIGN_UP_ERROR_LAST_NAME_SHORT));
            valid = false;
        } else if(lastname.length() > 12) {
            mTextInputLastName.setError(getString(R.string.SIGN_UP_ERROR_LAST_NAME_LONG));
            valid = false;
        } else {
            mTextInputLastName.setError(null);
        }

        if(password.isEmpty() || password.length() < 4) {
            mTextInputPassword.setError(getString(R.string.SIGN_UP_ERROR_PASSWORD_SHORT));
            valid = false;
        } else if(password.length() > 12) {
            mTextInputPassword.setError(getString(R.string.SIGN_UP_ERROR_PASSWORD_LONG));
            valid = false;
        } else {
            mTextInputPassword.setError(null);
        }

        if(confirm.isEmpty() || !confirm.equals(password)) {
            valid = false;
            mTextInputConfirm.setError(getString(R.string.SIGN_UP_ERROR_CONFIRM_INVALID));
        }

        return valid;
    }
}
