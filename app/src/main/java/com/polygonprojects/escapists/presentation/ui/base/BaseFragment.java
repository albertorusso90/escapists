package com.polygonprojects.escapists.presentation.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.polygonprojects.escapists.MyApplication;
import com.polygonprojects.escapists.di.fragments.HasFragmentSubcomponentBuilders;

/**
 * Created by Alberto Russo on 31/05/17.
 */

public abstract class BaseFragment extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupFragmentComponent();
    }

    protected void setupFragmentComponent() {
        injectMembers(MyApplication.getFragmentBuilder(getContext()));
    }

    protected abstract void injectMembers(HasFragmentSubcomponentBuilders hasFragmentSubcomponentBuilders);
}
