package com.polygonprojects.escapists.presentation.presenters;

import android.os.Handler;

import com.polygonprojects.escapists.di.fragments.FragmentScope;
import com.polygonprojects.escapists.models.Company;
import com.polygonprojects.escapists.models.DummyData;
import com.polygonprojects.escapists.models.Room;
import com.polygonprojects.escapists.networking.NetworkError;
import com.polygonprojects.escapists.networking.Service;
import com.polygonprojects.escapists.presentation.ui.base.BaseView;
import com.polygonprojects.escapists.presentation.ui.fragments.FragmentRooms;

import java.util.List;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Alberto Russo on 02/06/17
 */

@FragmentScope
public class PresenterRooms {

    private final Callback.View mView;
    private CompositeSubscription mSubscriptions;

    @Inject Service mService;

    public interface Callback{
        void onSuccess(List<Room> rooms);
        void onError(NetworkError networkError);

        interface View extends BaseView {
            void onRooms(List<Room> rooms);
        }
    }

    @Inject
    public PresenterRooms(FragmentRooms fragmentRooms) {
        this.mView = fragmentRooms;
        this.mSubscriptions = new CompositeSubscription();
    }

    public void getRooms(){
        mView.showProgress();

        Subscription subscription = mService.getRooms(new Callback() {
            @Override
            public void onSuccess(List<Room> rooms) {
                mView.hideProgress();
                mView.onRooms(rooms);
            }

            @Override
            public void onError(NetworkError networkError) {
//                mView.hideProgress();
//                mView.showError(networkError.getMessage());


                //TODO: Currently API is not working, so load dummy data. REMOVE THIS ON THE FUTURE!
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mView.hideProgress();

                        List<Company> companies = DummyData.getCompanies();
                        List<Room> rooms = Company.getAllRooms(companies);

                        mView.onRooms(rooms);
                    }
                }, 2000);
            }
        });

        mSubscriptions.add(subscription);
    }

    private void onStop() {
        mSubscriptions.unsubscribe();
    }
}
