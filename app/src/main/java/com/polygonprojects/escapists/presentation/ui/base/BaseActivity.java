package com.polygonprojects.escapists.presentation.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.polygonprojects.escapists.MyApplication;
import com.polygonprojects.escapists.di.activity.HasActivitySubcomponentBuilders;

/**
 * Created by Alberto Russo on 31/05/17.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActivityComponent();
    }

    protected void setupActivityComponent() {
        injectMembers(MyApplication.getActivityBuilder(this));
    }

    protected abstract void injectMembers(HasActivitySubcomponentBuilders hasActivitySubcomponentBuilders);

}
