package com.polygonprojects.escapists.presentation.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.polygonprojects.escapists.R;
import com.polygonprojects.escapists.models.Room;
import com.polygonprojects.escapists.presentation.ui.adapters.AdapterRoom;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Alberto Russo on 02/06/17
 */

public class FragmentRoomList extends Fragment{

    private static final String TAG = FragmentRoomList.class.getSimpleName();

    private FragmentRooms.Callback mCallback;
    private AdapterRoom mAdapter;

    @InjectView(R.id.recyclerView) RecyclerView mRecyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_room_list, container, false);
        ButterKnife.inject(this, view);

        init();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (FragmentRooms.Callback) context;
    }

    private void init() {
        mAdapter = new AdapterRoom(mCallback);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void refreshItems(List<Room> rooms) {
        if(mAdapter != null) {
            mAdapter.addItems(rooms);
        }
    }
}
