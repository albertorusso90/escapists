package com.polygonprojects.escapists.presentation.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.polygonprojects.escapists.R;

import butterknife.ButterKnife;

/**
 * Created by Alberto Russo on 02/06/17
 */

public class FragmentAbout extends Fragment {

    private static final String TAG = FragmentAbout.class.getSimpleName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        ButterKnife.inject(this, view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(R.string.ABOUT_TITLE);
    }
}
