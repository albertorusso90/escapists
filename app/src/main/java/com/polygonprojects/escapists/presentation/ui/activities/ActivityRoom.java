package com.polygonprojects.escapists.presentation.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.polygonprojects.escapists.R;
import com.polygonprojects.escapists.models.Room;
import com.polygonprojects.escapists.utils.Constants;
import com.polygonprojects.escapists.utils.Utils;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Alberto Russo on 03/06/17
 */

public class ActivityRoom extends AppCompatActivity {

    private static final String TAG = ActivityRoom.class.getSimpleName();

    private Room mRoom;

    @InjectView(R.id.toolbar) Toolbar mToolbar;
    @InjectView(R.id.textTitle) TextView mTextTitle;
    @InjectView(R.id.textDescription) TextView mTextDescription;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);
        ButterKnife.inject(this);

        init();
    }

    private void init() {
        setSupportActionBar(mToolbar);

        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(R.string.ROOM_DETAILS_TITLE);
        }

        mRoom = getIntent().getParcelableExtra(Constants.ARG_ROOM);
        if(mRoom != null) {
            mTextTitle.setText(mRoom.getName());
            mTextDescription.setText(mRoom.getDescription());
        } else {
            Utils.toast(this, getString(R.string.ROOM_DETAILS_ERROR_LOADING));
            onBackPressed();
        }
    }

    @OnClick(R.id.buttonInfo) public void onInfoButtonClicked() {
        Utils.toast(this, mRoom.getName());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
