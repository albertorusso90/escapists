package com.polygonprojects.escapists.models;

import com.polygonprojects.escapists.enums.Day;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Alberto Russo on 02/06/17
 */

public class DummyData {

    private Day[] days = new Day[] {Day.MONDAY, Day.TUESDAY, Day.WEDNESDAY, Day.THURSDAY, Day.FRIDAY, Day.SATURDAY, Day.SUNDAY};

    private List<Company> mCompanies = new ArrayList<>();

    private DummyData() {
        for(int i = 0; i < 20; i++) {
            Random random = new Random();

            Company company = new Company();
            company.setId(i + 1);
            company.setName("Company #" + (i + 1));

            company.setWebsite("http://" + "company" + (i + 1) + ".com");
            company.setEmail("company" + (i + 1) + "@foobar.com");
            company.setPhone("0031-64" + random.nextInt(9999999));

            String path = "http://escapists.nl/companies/" + company.getName() + "/";
            company.setLogo(path + "logo.jpg");
            company.setCover(path + "cover.jpg");

            for(int j = 0; j < 7; j++) {
                Opening opening = new Opening();
                opening.setCompanyId(company.getId());
                opening.setFrom("11:00");
                opening.setTill("19:00");
                opening.setDay(days[j]);

                company.addOpeningHour(opening);
            }

            for(int j = 0; j < random.nextInt(4) + 1; j++) {
                Room room = new Room();

                room.setId(j + 1);
                room.setCompanyId(company.getId());
                room.setName("Room #" + company.getId() + "." + room.getId());
                room.setDescription("Blah Blah Blah info for " + room.getName() + " in " + company.getName());
                room.setMinimumPlayers(2);
                room.setMaximumPlayers(random.nextBoolean() ? 8 : 4);
                room.setHasActors(random.nextBoolean());

                company.addRoom(room);
            }


            mCompanies.add(company);
        }


    }

    public static List<Company> getCompanies() {
        return new DummyData().mCompanies;
    }
}
