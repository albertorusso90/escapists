package com.polygonprojects.escapists.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alberto Russo on 02/06/17
 */

public class Company {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("logo")
    @Expose
    private String logo;

    @SerializedName("cover")
    @Expose
    private String cover;

    @SerializedName("website")
    @Expose
    private String website;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("opening_hours")
    @Expose
    private List<Opening> openingHours = new ArrayList<>();

    @SerializedName("opening_exceptions")
    @Expose
    private List<Opening> openingExceptions = new ArrayList<>();

    @SerializedName("rooms")
    @Expose
    private List<Room> rooms = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<Opening> getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(List<Opening> openingHours) {
        this.openingHours = openingHours;
    }

    public List<Opening> getOpeningExceptions() {
        return openingExceptions;
    }

    public void setOpeningExceptions(List<Opening> openingExceptions) {
        this.openingExceptions = openingExceptions;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public void addOpeningHour(Opening opening) {
        openingHours.add(opening);
    }

    public void addRoom(Room room) {
        this.rooms.add(room);
    }

    public static List<Room> getAllRooms(List<Company> companies) {
        List<Room> rooms = new ArrayList<>();

        for(Company company : companies) {
            rooms.addAll(company.getRooms());
        }

        return rooms;
    }

    @Override
    public String toString() {
        return getName() + " - " + getEmail() + " - " + getPhone();
    }
}
