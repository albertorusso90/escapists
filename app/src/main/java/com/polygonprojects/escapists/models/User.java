package com.polygonprojects.escapists.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Alberto Russo on 31/05/17.
 */

public class User {

    @SerializedName("id")
    @Expose(serialize = false)
    private int id;

    @SerializedName("firstName")
    @Expose
    private String firstName;

    @SerializedName("lastName")
    @Expose
    private String lastName;

    @SerializedName("emailAddress")
    @Expose(serialize = false)
    private String emailAddress;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName != null ? firstName : "";
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName != null ? lastName : "";
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getName() {
        String firstName = getFirstName();
        String lastName = getLastName();

        if(!firstName.equals("") && !lastName.equals("")) {
            return firstName + " " + lastName;
        }

        if(!firstName.equals("")) {
            return firstName;
        }

        if(!lastName.equals("")) {
            return lastName;
        }

        return "";
    }


    @Override
    public String toString() {
        return "Name: " + getName() +
                "; Email: " + getEmailAddress();
    }
}
