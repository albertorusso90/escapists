package com.polygonprojects.escapists.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Alberto Russo on 02/06/17
 */

public class Location {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("companyId")
    @Expose
    private int companyId;

    @SerializedName("street")
    @Expose
    private String street;

    @SerializedName("home_number")
    @Expose
    private int homeNumber;

    @SerializedName("postal_code")
    @Expose
    private String postalCode;

    @SerializedName("place")
    @Expose
    private String place;

    @SerializedName("location_mail")
    @Expose
    private String locationMail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(int homeNumber) {
        this.homeNumber = homeNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getLocationMail() {
        return locationMail;
    }

    public void setLocationMail(String locationMail) {
        this.locationMail = locationMail;
    }

    public String getAddress() {
        return getStreet() + " - " + getPostalCode() + " - " + getPlace();
    }

    @Override
    public String toString() {
        return getAddress();
    }
}
