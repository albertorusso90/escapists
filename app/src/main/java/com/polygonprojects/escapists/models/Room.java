package com.polygonprojects.escapists.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alberto Russo on 02/06/17
 */

public class Room implements Parcelable{

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("companyId")
    @Expose
    private int companyId;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("theme")
    @Expose
    private Object theme; //TODO

    @SerializedName("tags")
    @Expose
    private Object tags; //TODO

    @SerializedName("min_players")
    @Expose
    private int minimumPlayers;

    @SerializedName("max_players")
    @Expose
    private int maximumPlayers;

    @SerializedName("languages")
    @Expose
    private List<Object> languages = new ArrayList<>(); //TODO

    @SerializedName("status")
    @Expose
    private Object status; //TODO

    @SerializedName("reset_time")
    @Expose
    private String resetTime;

    @SerializedName("with_actors")
    @Expose
    private boolean hasActors;

    public Room() {}

    private Room(Parcel in){
        id = in.readInt();
        name = in.readString();
        description = in.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getTheme() {
        return theme;
    }

    public void setTheme(Object theme) {
        this.theme = theme;
    }

    public Object getTags() {
        return tags;
    }

    public void setTags(Object tags) {
        this.tags = tags;
    }

    public int getMinimumPlayers() {
        return minimumPlayers;
    }

    public void setMinimumPlayers(int minimumPlayers) {
        this.minimumPlayers = minimumPlayers;
    }

    public int getMaximumPlayers() {
        return maximumPlayers;
    }

    public void setMaximumPlayers(int maximumPlayers) {
        this.maximumPlayers = maximumPlayers;
    }

    public List<Object> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Object> languages) {
        this.languages = languages;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public String getResetTime() {
        return resetTime;
    }

    public void setResetTime(String resetTime) {
        this.resetTime = resetTime;
    }

    public boolean isHasActors() {
        return hasActors;
    }

    public void setHasActors(boolean hasActors) {
        this.hasActors = hasActors;
    }

    @Override
    public String toString() {
        return getName() + " - " + getDescription();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(name);
        out.writeString(description);
    }

    public static final Parcelable.Creator<Room> CREATOR = new Parcelable.Creator<Room>() {

        @Override
        public Room createFromParcel(Parcel source) {
            return new Room(source);
        }

        @Override
        public Room[] newArray(int size) {
            return new Room[size];
        }
    };
}
