package com.polygonprojects.escapists.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.polygonprojects.escapists.enums.Day;

/**
 * Created by Alberto Russo on 02/06/17
 */

public class Opening {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("companyId")
    @Expose
    private int companyId;

    @SerializedName("day")
    @Expose
    private Day day;

    @SerializedName("from")
    @Expose
    private String from;

    @SerializedName("till")
    @Expose
    private String till;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTill() {
        return till;
    }

    public void setTill(String till) {
        this.till = till;
    }

    @Override
    public String toString() {
        return getDay().toString() + " " + getFrom() + " - " + getTill();
    }
}
