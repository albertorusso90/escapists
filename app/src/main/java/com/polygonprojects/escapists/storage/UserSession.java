package com.polygonprojects.escapists.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.polygonprojects.escapists.models.User;
import com.polygonprojects.escapists.utils.Constants;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alberto Russo on 31/05/17.
 */

public class UserSession {

    private static final String KEY_EMAIL       = "key_email";
    private static final String KEY_PASSWORD    = "key_password";
    private static final String KEY_TOKEN       = "key_token";

    private SharedPreferences mPrefs;

    public UserSession(Context context) {
        this.mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getEmail() {
        return mPrefs.getString(KEY_EMAIL, null);
    }

    private void setEmail(String email) {
        setPreferences(KEY_EMAIL, email);
    }

    public String getPassword() {
        return mPrefs.getString(KEY_PASSWORD, null);
    }

    private void setPassword(String password) {
       setPreferences(KEY_PASSWORD, password);
    }

    public String getToken() {
        return mPrefs.getString(KEY_TOKEN, null);
    }

    public void setToken(String token) {
        setPreferences(KEY_TOKEN, token);
    }

    private void setPreferences(String key, String value) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    private void setPreferences(String key, boolean value) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public Map<String, String> getUser() {
        Map<String, String> fields = new HashMap<>();
        fields.put(Constants.KEY_EMAIL, getEmail());
        fields.put(Constants.KEY_PASSWORD, getPassword());

        return fields;
    }

    public void saveUser(User user, String password, String token) {
        setEmail(user.getEmailAddress());
        setPassword(password);
        setToken(token);
    }

    public void deleteUser() {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.remove(KEY_EMAIL);
        editor.remove(KEY_PASSWORD);
        editor.remove(KEY_TOKEN);
        editor.apply();
    }

    public boolean hasUser() {
        String email = getEmail();
        String password = getPassword();

        return email != null && password != null;
    }
}
