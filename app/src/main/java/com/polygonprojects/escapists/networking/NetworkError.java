package com.polygonprojects.escapists.networking;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;

/**
 * Created by Alberto Russo on 31/05/17
 */

public class NetworkError {

    @SerializedName("statusCode")
    @Expose
    private int statusCode;

    @SerializedName("error")
    @Expose
    private String error;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("validation")
    @Expose
    private Validation validation;

    public NetworkError(String message) {
        this.message = message;
    }

    public NetworkError(Throwable throwable) {
        this.message = throwable.getMessage();
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Validation getValidation() {
        return validation;
    }

    public void setValidation(Validation validation) {
        this.validation = validation;
    }

    public class Validation {

        @SerializedName("payload")
        @Expose
        private String payload;

        @SerializedName("keys")
        @Expose
        private List<String> keys = new ArrayList<>();

        public String getPayload() {
            return payload;
        }

        public void setPayload(String payload) {
            this.payload = payload;
        }

        public List<String> getKeys() {
            return keys;
        }

        public void setKeys(List<String> keys) {
            this.keys = keys;
        }
    }

    public static NetworkError convertToNetworkError(ResponseBody responseBody) {
        NetworkError networkError;

        try {
            networkError = new Gson().fromJson(responseBody.string(), NetworkError.class);
        } catch (IOException e) {
            e.printStackTrace();

            networkError = new NetworkError("Something went wrong...");
        }


        return networkError;
    }

    @Override
    public String toString() {
        return "statusCode: " + getStatusCode() +
                "; error: " + getError() +
                "; message: " + getMessage();
    }
}
