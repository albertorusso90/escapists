package com.polygonprojects.escapists.networking;


import com.polygonprojects.escapists.models.Room;
import com.polygonprojects.escapists.models.User;
import com.polygonprojects.escapists.presentation.presenters.PresenterRooms;
import com.polygonprojects.escapists.presentation.presenters.PresenterSign;

import java.util.List;
import java.util.Map;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Alberto Russo on 31/05/17.
 */

public class Service {
    private static final String TAG = Service.class.getSimpleName();

    private final NetworkService networkService;

    public Service(NetworkService networkService) {
        this.networkService = networkService;
    }

    public Subscription login(Map<String, String> fields, final PresenterSign.Callback callback) {
        return networkService.login(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends Response<User>>>() {
                    @Override
                    public Observable<? extends Response<User>> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<Response<User>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        NetworkError networkError = new NetworkError(e);
                        callback.onError(networkError);
                    }

                    @Override
                    public void onNext(Response<User> userResponse) {
                        if(userResponse.isSuccessful()) {
                            callback.onSuccess(userResponse);
                        } else {
                            NetworkError networkError = NetworkError.convertToNetworkError(userResponse.errorBody());
                            callback.onError(networkError);
                        }
                    }
                });
    }

    public Subscription register(Map<String, String> fields, final PresenterSign.Callback callback) {
        return networkService.register(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends Response<User>>>() {
                    @Override
                    public Observable<? extends Response<User>> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<Response<User>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        NetworkError networkError = new NetworkError(e);
                        callback.onError(networkError);
                    }

                    @Override
                    public void onNext(Response<User> userResponse) {
                        if(userResponse.isSuccessful()) {
                            callback.onSuccess(userResponse);
                        } else {
                            NetworkError networkError = NetworkError.convertToNetworkError(userResponse.errorBody());
                            callback.onError(networkError);
                        }
                    }
                });
    }

    public Subscription getRooms(final PresenterRooms.Callback callback) {
        return networkService.getRooms()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends Response<List<Room>>>>() {
                    @Override
                    public Observable<? extends Response<List<Room>>> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<Response<List<Room>>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));
                    }

                    @Override
                    public void onNext(Response<List<Room>> roomsResponse) {
                        if(roomsResponse.isSuccessful()) {
                            callback.onSuccess(roomsResponse.body());
                        } else {
                            NetworkError networkError = NetworkError.convertToNetworkError(roomsResponse.errorBody());
                            callback.onError(networkError);
                        }
                    }
                });
    }

}
