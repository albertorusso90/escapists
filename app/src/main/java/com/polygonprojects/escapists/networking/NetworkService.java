package com.polygonprojects.escapists.networking;

import com.polygonprojects.escapists.models.Room;
import com.polygonprojects.escapists.models.User;

import java.util.List;
import java.util.Map;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import rx.Observable;

/**
 * Created by Alberto Russo on 31/05/17.
 */

public interface NetworkService {

    @FormUrlEncoded
    @POST("/login")
    Observable<Response<User>> login(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST("/register")
    Observable<Response<User>> register(@FieldMap Map<String, String> fields);

    @GET("/me")
    Observable<Response<User>> getUser();

    @PUT("/me")
    Observable<Void> updateUser(@Body User user);

    @GET("/rooms")
    Observable<Response<List<Room>>> getRooms();
}
