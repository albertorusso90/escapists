package com.polygonprojects.escapists.enums;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alberto Russo on 02/06/17
 */

public enum Day {
    @SerializedName("MONDAY")
    MONDAY("Monday"),

    @SerializedName("TUESDAY")
    TUESDAY("Tuesday"),

    @SerializedName("WEDNESDAY")
    WEDNESDAY("Wednesday"),

    @SerializedName("THURSDAY")
    THURSDAY("Thursday"),

    @SerializedName("FRIDAY")
    FRIDAY("Friday"),

    @SerializedName("SATURDAY")
    SATURDAY("Saturday"),

    @SerializedName("SUNDAY")
    SUNDAY("Sunday");

    private String code;

    Day(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static Day getEnum(String code){
        switch (code) {
            case "Monday":
                return MONDAY;
            case "Tuesday":
                return TUESDAY;
            case "Wednesday":
                return WEDNESDAY;
            case "Thursday":
                return THURSDAY;
            case "Friday":
                return FRIDAY;
            case "Saturday":
                return SATURDAY;
            case "Sunday":
                return SUNDAY;
            default:
                return null;
        }
    }
}
